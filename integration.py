#!/usr/bin/env python3

import logging
import requests
import pandas as pd

logging.basicConfig()
logger = logging.getLogger(f"{__name__}")
logger.setLevel(logging.DEBUG)


def main():
    db_endpoint = "http://localhost:8000/"
    active_customers = db_endpoint + "customers?active=true"
    forward_endpoint = "https://postman-echo.com/post"
    req_timeout = 20

    # try to download the data from the db endpoint
    try:
        data = requests.get(active_customers, timeout=req_timeout)
    except (requests.Timeout, requests.ConnectionError) as exc:
        logger.error("GET request error to %s: %s", active_customers, exc)
        raise

    # if we don't get an OK from the request
    if data.status_code != 200:
        logger.error("GET response not OK")
        # raise a generic requests exc
        raise requests.exceptions.RequestException

    # try to construct new dataframe from data
    try:
        dframe = pd.read_json(data.json(), convert_dates=False)
    except ValueError as exc:
        # both pd.read_json and response.json raise JSONDecoderError
        # which inherits ValueError
        logger.error("Failed to load JSON into data frame: %s", exc)
        raise

    # create 'name' field from 'surname' and 'firstname'
    dframe["name"] = dframe["firstname"] + " " + dframe["surname"]
    logger.debug(dframe)

    # encode the data as JSON
    try:
        json = dframe.to_json()
    except ValueError as exc:
        logger.error("Unable to encode data as JSON: %s", exc)
        raise

    if json:
        try:
            resp = requests.post(forward_endpoint, json=json, timeout=req_timeout)
            logger.info("POST request response code %s", resp.status_code)
        except (requests.Timeout, requests.ConnectionError) as exc:
            logger.error("POST request error to %s: %s", forward_endpoint, exc)
            raise


if __name__ == "__main__":
    main()
