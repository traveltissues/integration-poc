#!/bin/bash

podman run -d \
  --name scratchdb \
  -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
  -p 5432:5432 \
  docker.io/postgres:latest
