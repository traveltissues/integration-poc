#!/usr/bin/env python3

import pandas as pd

from dbconhandler import DBHandler


def load_csv(table_data, engine):
    """load CSV data and insert into new tables in the DB
    based on a mapping of table name to file and index column"""
    for table_name, data in table_data.items():
        print(
            f'Loading data into {table_name} from {data["path"]} '
            f'indexed to column {data["index"]}'
        )

        # create a dataframe from the csv
        dframe = pd.read_csv(data["path"], index_col=data["index"], header=0)

        # try to execute table creation
        try:
            dframe.to_sql(table_name, engine, if_exists="replace")
        except ValueError as exc:
            print(
                f'failed to write table data to {table_name} from {data["path"]}: {exc}'
            )


def main():
    table_data = {
        "customers": {"index": 0, "path": "customers.csv"},
        "orders": {"index": 0, "path": "orders.csv"},
    }

    with DBHandler() as engine:
        load_csv(table_data, engine)


if __name__ == "__main__":
    main()
