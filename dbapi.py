import logging
from typing import Union
import pandas as pd
from fastapi import FastAPI
from sqlalchemy import text

from dbconhandler import DBHandler

# setup some basic logging
logging.basicConfig()
logger = logging.getLogger(f"{__name__}")
logger.setLevel(logging.DEBUG)

dbapp = FastAPI()  # make a dummy application


def make_customer_query():
    """Construct the query for all customers joined to orders"""
    # using a left join here to return customers that have no matching orders
    return (
        "SELECT t1.customer_id, t1.firstname, t1.surname, t1.email, "
        "t1.address, t1.zip_code, t1.region, t1.status, t2.order_id, "
        "t2.date, t2.amount FROM customers AS t1 LEFT JOIN orders AS t2 "
        "ON t1.customer_id = t2.customer_id"
    )


def get_query_json(query, params=None):
    """Execute DB query and return the result in JSON"""
    json_data = []

    with DBHandler() as engine:
        logger.info("Executing query: %s with params: %s", query, params)
        try:
            dframe = pd.read_sql_query(query, con=engine, params=params)
            logger.debug(dframe)
            json_data = dframe.to_json()
        except ValueError as exc:
            logger.error("Failed to execute query or encode JSON: %s", exc)

    return json_data


@dbapp.get("/customers")
def get_customers(active: Union[bool, None] = None):
    """Return all customer/order data
    Takes an optional parameter 'active' to filter
    customers by status
    """
    query = make_customer_query()

    # reduce the set by status
    if active is True:
        query += " WHERE status = 'active'"
    elif active is False:
        query += " WHERE status = 'archived'"

    return get_query_json(text(query))


@dbapp.get("/customers/{customer_id}")
def get_customer_by_id(customer_id: int):
    """Return customer/order data for a
    specific customer_id
    """
    query = make_customer_query() + " WHERE t1.customer_id = :customer_id"
    params = {"customer_id": customer_id}

    return get_query_json(text(query), params)
