import os
import logging

from sqlalchemy import create_engine, exc


class DBHandler:
    """holds the DB engine which is used for managing connections to the DB"""

    DB_PARAMS = {
        "host": "localhost",  # for locally running container
        "database": "postgres",  # default DB
        "user": "postgres",  # default user
        "port": 5432,  # default port
        "password": os.getenv("POSTGRES_PASSWORD"),
    }

    def __init__(self, db_params=None):
        """set up the parameters for the DB connection and basic logging"""
        if db_params is None:
            db_params = self.DB_PARAMS
        self._db_params = db_params
        self._engine = None
        logging.basicConfig()
        self._logger = logging.getLogger(f"{__name__}.{self.__class__.__name__}")
        self._logger.setLevel(logging.INFO)

    @property
    def engine(self):
        if self._engine is None:
            self.create_engine()
        return self._engine

    def __enter__(self):
        return self.engine

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.engine.dispose()
        self._logger.info("Disposed of engine %s", self.engine)

    def create_engine(self):
        connection_str = (
            f'postgresql+psycopg2://{self._db_params["user"]}:'
            f'{self._db_params["password"]}@{self._db_params["host"]}:'
            f'{self._db_params["port"]}/{self._db_params["database"]}'
        )
        try:
            engine = create_engine(connection_str)
        except exc.SQLAlchemyError as err:
            self._logger.error("Failed to create engine: %s", err)
            engine = None

        self._logger.info("Created engine: %s", engine)
        self._engine = engine
